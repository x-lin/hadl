package ac.at.tuwien.dsg.hadl.framework.test.runtime.surrogates;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.AbstractAsyncCollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ESurrogateStatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;


public class DefaultAcceptingCollaboratorSurrogate extends AbstractAsyncCollaboratorSurrogate  {

	private void sleepAndFire(BehaviorSubject<SurrogateEvent> subject, SurrogateEvent se)
	{
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		subject.onNext(se);	
		subject.onCompleted();
	}
	
	@Override
	public void asyncBegin(BehaviorSubject<SurrogateEvent> subject) {
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, oc1 == null ? oc2 : oc2));		
	}

	@Override
	public void asyncStop(BehaviorSubject<SurrogateEvent> subject) {
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, oc1 == null ? oc2 : oc1));
	}

	@Override
	public void asyncRelease(BehaviorSubject<SurrogateEvent> subject) {
		if (super.oc1 != null)
		{
			oc1.setState(TOperationalState.DESCRIBED_NONEXISTING);
			sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS, oc1));
			oc1 = null;
		}
		if (super.oc2 != null)
		{
			oc2.setState(TOperationalState.DESCRIBED_NONEXISTING);
			sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS, oc2));
			oc2 = null;
		}		
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalComponent surrogateFor,
			BehaviorSubject<SurrogateEvent> subject) {				
		oc1.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, oc1));
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalConnector surrogateFor,
			BehaviorSubject<SurrogateEvent> subject) {				
		oc2.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, oc2));
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalObject oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));		
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalObject oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_NONEXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public void asyncRelating(TOperationalConnector oppositeElement,
			TCollabRef relationType, boolean isRelationOrigin,
			boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabRef ref) {
		if (doRemove) 
			ref.setState(TOperationalState.DESCRIBED_NONEXISTING);
		else
			ref.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref) );
	}

	@Override
	public void asyncRelating(TOperationalComponent oppositeElement,
			TCollabRef relationType, boolean isRelationOrigin,
			boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabRef ref) {
		if (ref!= null)
		{
			if (doRemove) 
				ref.setState(TOperationalState.DESCRIBED_NONEXISTING);
			else
				ref.setState(TOperationalState.DESCRIBED_EXISTING);
		}
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
	}



}
