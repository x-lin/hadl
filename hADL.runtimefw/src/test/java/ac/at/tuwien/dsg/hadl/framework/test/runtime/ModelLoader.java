package ac.at.tuwien.dsg.hadl.framework.test.runtime;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.ObjectFactory;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TStructure;
import at.ac.tuwien.dsg.hadl.schema.runtime.TRuntimeStructure;


public class ModelLoader 
{

	
	
	public static HADLmodel loadGoogleModel()
	{
		try {
			JAXBContext jc = JAXBContext.newInstance( ObjectFactory.class,
					at.ac.tuwien.dsg.hadl.schema.executable.ObjectFactory.class); 
					//, at.ac.tuwien.dsg.adaf.hADLmodel.schema.runtime.ObjectFactory.class);

			Unmarshaller unmarshaller = jc.createUnmarshaller();			
			File xml = new File("../hADL.schema/src/main/resources/examples/GoogleDrive.xml");
			return (HADLmodel) unmarshaller.unmarshal(xml);
			
		} catch (JAXBException e) {			
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static TActivityScope retrieveScopeById(HADLmodel model, String scopeId)
	{
		for (TStructure struct : model.getHADLstructure())
		{
			for (TActivityScope actS : struct.getActivityScope())
			{
				if (actS.getId().equals(scopeId))
					return actS;
			}
		}
		return null;
	}
	
	public static void storeRuntimeModel(TRuntimeStructure struct, String outputTo)
	{
		try {
			at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory of = new at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory();
			JAXBContext jc = JAXBContext.newInstance("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.runtime:at.ac.tuwien.dsg.hadl.schema.extension.google");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal(of.createRuntimeStructure(struct), new File(".\\src\\test\\resources\\output\\"+outputTo));
		} catch (JAXBException e) {			
			e.printStackTrace();
		}
	}
	
	public static void storeSpecModel(HADLmodel model, String outputTo)
	{
		try {			
			JAXBContext jc = JAXBContext.newInstance("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.executable");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal(model, new File(".\\src\\test\\resources\\output\\"+outputTo));
		} catch (JAXBException e) {			
			e.printStackTrace();
		}
	}
	
}
