package ac.at.tuwien.dsg.hadl.framework.test.runtime;

import static org.junit.Assert.*;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.schedulers.Schedulers;


import at.ac.tuwien.dsg.hadl.framework.runtime.impl.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;

/**
 * @author work
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HADLLinkageConnectorTest {

	private Observable<StatusEvent> obsScope;	
	private Injector injector;
	private HADLLinkageConnector app;
	private TActivityScope scope;
	private ExecutorService dispatcher;
	private HADLmodel model;
	private CountDownLatch scopeLock;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		if (dispatcher == null || dispatcher.isShutdown())
			dispatcher = Executors.newSingleThreadExecutor();
		model = ModelLoader.loadGoogleModel();	
		assertNotNull(model);
		scope = ModelLoader.retrieveScopeById(model, "DefaultFullScope");		
		assertNotNull(scope);
		
		scopeLock = new CountDownLatch(1);
		
		final Injector baseInjector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {				
			}
			
			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return new Executable2OperationalTransformer();
			}
			
			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{
				ModelTypesUtil mtu = new ModelTypesUtil();
				mtu.init(model);
				return mtu;
			}
		});
		
		injector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				bind(HADLLinkageConnector.class).in(Singleton.class);				
			}			
			
			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return baseInjector.getInstance(Executable2OperationalTransformer.class);
			}
			
			@Provides
			@Singleton
			RuntimeRegistry getRegistry()
			{
				return new RuntimeRegistry();				
			}
			
			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{
				return baseInjector.getInstance(ModelTypesUtil.class);
			}
			
			@Provides
			@Singleton
			SurrogateFactoryResolver getFactoryResolver()
			{
				SurrogateFactoryResolver sfr = new SurrogateFactoryResolver();
				try {
					sfr.resolveFactoriesFrom(model);
				} catch (FactoryResolvingException e) {					
					e.printStackTrace();
				}
				return sfr; 				
			}
			
			@Provides
			@Singleton
			HADLruntimeModel getRuntimeModel()
			{
				HADLruntimeModel model = new HADLruntimeModel();
				baseInjector.injectMembers(model);
				return model; 							
			}
			
		});			
		app = injector.getInstance(HADLLinkageConnector.class);
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
		app.shutdown();
		try {
			assertTrue("Scope Timeout", scopeLock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}		
		injector = null;
		scope = null;		
		app = null;
		obsScope = null;		
		dispatcher.shutdownNow();
		dispatcher = null;
	}


	@Test	
	public void test_1_Init() {
						
		obsScope = app.init(scope);
		obsScope.subscribe(new Observer<StatusEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("HADLLinkageConnector completed");	
				scopeLock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("HADLLinkageConnector encountered error");
				e.printStackTrace();				
				fail();
				scopeLock.countDown();
			}

			@Override
			public void onNext(StatusEvent t) {				
				System.out.println(t.getStatus());
				assertFalse("Status INCOMPLETED", t.getStatus().toString().endsWith("INCOMPLETED"));
			}			
		});		
	}			

	@Test
	public void test_2_Acquire() {
		
		test_1_Init();
		ModelTypesUtil mtu = injector.getInstance(ModelTypesUtil.class);
		THADLarchElement userType = mtu.getById("GoogleDriveUser");
		assertNotNull(userType);
		THADLarchElement fileType = mtu.getById("GoogleFile");
		assertNotNull(fileType);
		TGoogleFileDescriptor gfd1 = new TGoogleFileDescriptor();
		gfd1.setName("Nonexistant file1");
		TGoogleFileDescriptor gfd2 = new TGoogleFileDescriptor();
		gfd2.setName("Nonexistant file2");
		TGoogleUserDescriptor gud1 = new TGoogleUserDescriptor();
		gud1.setEmail("someone@google.com");
		gud1.setId("googleId1");
		TGoogleUserDescriptor gud2 = new TGoogleUserDescriptor();
		gud2.setEmail("someone2@google.com");
		gud2.setId("googleId2");
		
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();		
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(fileType, gfd1));
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(fileType, gfd2));
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(userType, gud1));
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(userType, gud2));
		
		final CountDownLatch lock = new CountDownLatch(1);
		Observable<SurrogateEvent> obs = app.acquireResourcesAsElements(mapping);
		obs.subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("Acquire completed");
				HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 
				ModelLoader.storeRuntimeModel(hrm, "test2acquire.xml");
				lock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();				
				fail("Surrogate Observer obtained Exception");
				lock.countDown();
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println("> "+t.getStatus() +" : "+t.getSource().getHref());
			}			
		});
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@Test
	public void test_3_CreateRelations() {
		test_2_Acquire();
		final HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 	
		List<Entry<TOperationalObject, TOperationalObject>> fromTo = new ArrayList<Map.Entry<TOperationalObject,TOperationalObject>>();
		fromTo.add(new AbstractMap.SimpleEntry<TOperationalObject, TOperationalObject>(hrm.getObject().get(0), hrm.getObject().get(1)));
		
		final CountDownLatch lock = new CountDownLatch(1);
		Observable<SurrogateEvent> obs = app.buildRelations(fromTo , model.getHADLstructure().get(0).getObjectRef().get(2), false);
		obs.subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("Creating Relations completed");	
				ModelLoader.storeRuntimeModel(hrm, "test3relations.xml");
				lock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();
				lock.countDown();
				fail("Surrogate Observer obtained Exception");
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println(t.getStatus() +" : "+t.getSource().getHref());
			}			
		});	
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}		
	}
	
	@Test
	public void test_3_RemoveRelations() {		
		test_3_CreateRelations();
		final HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 	
		List<Entry<TOperationalObject, TOperationalObject>> fromTo = new ArrayList<Map.Entry<TOperationalObject,TOperationalObject>>();
		fromTo.add(new AbstractMap.SimpleEntry<TOperationalObject, TOperationalObject>(hrm.getObject().get(0), hrm.getObject().get(1)));
		
		final CountDownLatch lock = new CountDownLatch(1);
		Observable<SurrogateEvent> obs = app.buildRelations(fromTo , model.getHADLstructure().get(0).getObjectRef().get(2), true);
		obs.subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("Removing Relations completed");	
				ModelLoader.storeRuntimeModel(hrm, "test3relations.xml");
				ModelLoader.storeSpecModel(model, "inputSpec.xml");
				lock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();				
				fail("Surrogate Observer obtained Exception");
				lock.countDown();
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println(t.getStatus() +" : "+t.getSource().getHref());
			}			
		});	
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}	
	}
	
	@Test
	public void test_3_Wire() {
				
		test_2_Acquire();
		final HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 	
		final CountDownLatch lock = new CountDownLatch(1);
//		Observable<SurrogateEvent> obs = app.wireAllActions();
		Observable<SurrogateEvent> obs = app.wireActionsOfObjectTypes(Arrays.asList(scope.getObjectRef().get(0)), Arrays.asList(scope.getLinkRef().get(2)));
		obs.subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("WireUp completed");	
				ModelLoader.storeRuntimeModel(hrm, "test3wires.xml");
				lock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();
				fail("Surrogate Observer obtained Exception");
				lock.countDown();
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println(t.getStatus() +" : "+t.getSource().getHref());
			}			
		});
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}	
	}
	
	@Test
	public void test_4_StopAndUnwire() {
				
		test_3_Wire();
		
		Observable<SurrogateEvent> obs = app.stopScope();
		Observable<SurrogateEvent> obs2 = app.unwireAllActions();
		HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 		
		int count = 0;
		for (TOperationalCollabLink link : hrm.getLink()) 
		{
			if (!link.getState().equals(TOperationalState.PRESCRIBED_NONEXISTING))
				count++;
		}
		assertEquals("Count of Links not in PrescribedNonExisting", 0, count);		
		final CountDownLatch lock = new CountDownLatch(1);
		Observable.merge(obs2, obs).subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("Unwire completed");
				lock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();				
				fail("Surrogate Observer obtained Exception");
				lock.countDown();
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println(t.getStatus() +" : "+t.getSource().getHref());
			}			
		});	
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}	
	}
	
	@Test
	public void test_5_Disolve()
	{
		test_4_StopAndUnwire();				
		Observable<SurrogateEvent> obs = app.releaseComponentResources();
		final CountDownLatch lock = new CountDownLatch(1);
		obs.subscribe(new Observer<SurrogateEvent>() {

			@Override
			public void onCompleted() {	
				System.out.println("Released Components");
				app.releaseConnectorResources().subscribe(new Observer<SurrogateEvent>() {

					@Override
					public void onCompleted() {	
						System.out.println("Released Connectors");
						app.releaseObjectResources().subscribe(new Observer<SurrogateEvent>() {

							@Override
							public void onCompleted() {	
								System.out.println("Releasing completed");	
								HADLruntimeModel hrm = injector.getInstance(HADLruntimeModel.class); 
								ModelLoader.storeRuntimeModel(hrm, "test5released.xml");		
								lock.countDown();
							}

							@Override
							public void onError(Throwable e) {
								System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
								e.printStackTrace();
								fail("Surrogate Observer obtained Exception");
								lock.countDown();
							}

							@Override
							public void onNext(SurrogateEvent t) {				
								System.out.println(t.getStatus() +" : "+t.getSource().getHref());
							}			
						});	
																
					}

					@Override
					public void onError(Throwable e) {
						System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
						e.printStackTrace();
						fail("Surrogate Observer obtained Exception");
						lock.countDown();
					}

					@Override
					public void onNext(SurrogateEvent t) {				
						System.out.println(t.getStatus() +" : "+t.getSource().getHref());
					}			
				});	 
														
			}

			@Override
			public void onError(Throwable e) {
				System.out.println("Surrogate(s) encountered error: "+e.getMessage()+ "\r\n"); 
				e.printStackTrace();
				fail("Surrogate Observer obtained Exception");
				lock.countDown();
			}

			@Override
			public void onNext(SurrogateEvent t) {				
				System.out.println(t.getStatus() +" : "+t.getSource().getHref());
			}			
		});	
		
		try {
			assertTrue("Timeout", lock.await(5000, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}	
	}
}
