package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

public class FactoryResolvingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8327964666257789345L;

	public FactoryResolvingException() {
		super();	
	}

	public FactoryResolvingException(String arg0, Throwable arg1) {
		super(arg0, arg1);		
	}

	public FactoryResolvingException(String arg0) {
		super(arg0);
	}

	public FactoryResolvingException(Throwable arg0) {
		super(arg0);
	}

	
	
}
