package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessage;

import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory;



public class HADLruntimeModel extends TRuntimeStructure
{
	protected static final Logger logger = LogManager.getLogger(HADLruntimeModel.class);
	
	private Hashtable<String,THADLarchElement> elements = new Hashtable<String,THADLarchElement>();
	@Inject private Executable2OperationalTransformer e2o;
	@Inject private ModelTypesUtil mtu; 
	
	public void addComponent(TOperationalComponent oc)
	{
		elements.put(oc.getId(), oc);
		super.getComponent().add(oc);
		if (oc.getInstanceOf() != null)
		{
			List<TOperationalComponent> list = new ArrayList<TOperationalComponent>();
			if (hADLType2compInst.containsKey(oc.getInstanceOf()))
			{
				list = hADLType2compInst.get(oc.getInstanceOf());
			}
			else
			{
				hADLType2compInst.put(oc.getInstanceOf(), list);
			}
			list.add(oc);			
		}			
	}
	
	public THADLarchElement getOperationalElement(String id)
	{
		return elements.get(id);
	}
	
	// returns a readonly list!
	public List<TOperationalComponent> getComponent() {
		return Collections.unmodifiableList(super.getComponent());
	}

	public String getName() {
		return super.getName();
	}

	public void setName(String value) {
		super.setName(value);
	}

	public String getDescription() {
		return super.getDescription();
	}
	
	public void addConnector(TOperationalConnector oc)
	{
		elements.put(oc.getId(), oc);
		super.getConnector().add(oc);
		if (oc.getInstanceOf() != null)
		{
			List<TOperationalConnector> list = new ArrayList<TOperationalConnector>();
			if (hADLType2connInst.containsKey(oc.getInstanceOf()))
			{
				list = hADLType2connInst.get(oc.getInstanceOf());
			}
			else
			{
				hADLType2connInst.put(oc.getInstanceOf(), list);
			}
			list.add(oc);			
		}
	}
	
	// returns a readonly list!
	public List<TOperationalConnector> getConnector() {
		return Collections.unmodifiableList(super.getConnector());
	}

	public void setDescription(String value) {
		super.setDescription(value);
	}

	public List<Extension> getExtension() {
		return super.getExtension();
	}

	public void addObject(TOperationalObject oo)
	{
		elements.put(oo.getId(), oo);
		super.getObject().add(oo);
		if (oo.getInstanceOf() != null)
		{			
			List<TOperationalObject> list = new ArrayList<TOperationalObject>();
			if (hADLType2objInst.containsKey(oo.getInstanceOf()))
			{
				list = hADLType2objInst.get(oo.getInstanceOf());
			}
			else
			{
				hADLType2objInst.put(oo.getInstanceOf(), list);
			}
			list.add(oo);			
		}
	}
		
	// returns a readonly list!
	public List<TOperationalObject> getObject() {
		return Collections.unmodifiableList(super.getObject());
	}

	public String getId() {
		return super.getId();
	}

	public void setId(String value) {
		super.setId(value);
	}

	// returns a readonly list!
	public List<TOperationalCollabLink> getLink() {
		return Collections.unmodifiableList(super.getLink());
	}

	public TOperationalCollabLink addLink(TOperationalComponent comp, TOperationalObject obj, TCollabLink wireType)
	{
		if (elements.containsKey(comp.getId()) && elements.containsKey(obj.getId()))
		{			
			return addLink(wireType, getOrCreateAction(comp, wireType), getOrCreateAction(obj, wireType), obj, comp, TOperationalState.PRESCRIBED_EXISTING);			
		}				
		return null;
	}
	
	public void renewLink(TOperationalCollabLink link)
	{
		link.setState(TOperationalState.PRESCRIBED_EXISTING);
	}
	
	public TOperationalCollabLink addLink(TOperationalConnector conn, TOperationalObject obj, TCollabLink wireType)
	{
		if (elements.containsKey(conn.getId()) && elements.containsKey(obj.getId()) && !(wireType == null))
		{			
			return addLink(wireType, getOrCreateAction(conn, wireType), getOrCreateAction(obj, wireType), obj, conn, TOperationalState.PRESCRIBED_EXISTING);			
		}				
		return null;
	}
	
	public TOperationalCollabLink addLinkIfNotExisting(TOperationalConnector conn, TOperationalObject obj, TCollabLink wireType)
	{
		if (conn == null || obj == null || wireType == null)
		{
			logger.warn(new MessageFormatMessage("Cannot add TOperationalCollabLink if any of TOperationalConnector {0}, TOperationalObject {1}, or TCollabLink {2} is null.", new Object[]{conn == null ? "NULL" : conn.getId(), obj == null ? "NULL" : obj.getId(), wireType == null ? "NULL" : wireType.getId()}));
			return null;
		}			
		TOperationalCollabLink link = getOperationalLinkForPairAndType(new ImmutableTriple<TOperationalObject, THADLarchElement, TCollabLink>(obj, conn, wireType));
		if (link == null)
			return addLink(wireType, getOrCreateAction(conn, wireType), getOrCreateAction(obj, wireType), obj, conn, TOperationalState.DESCRIBED_EXISTING);
		else
		{
			if (link.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || // we want the link to exist, there it is 
					link.getState().equals(TOperationalState.DESCRIBED_NONEXISTING) || // we confirmed it as non existing before, now its back 
					link.getState().equals(TOperationalState.PRESCRIBED_EXISTING_DENIED) // initially denied, now it does exist for whatever reasons
					)
				link.setState(TOperationalState.DESCRIBED_EXISTING);	
		}
		return link; 
	}
	
	public TOperationalCollabLink addLinkIfNotExisting(TOperationalComponent comp, TOperationalObject obj, TCollabLink wireType)
	{
		if (comp == null || obj == null || wireType == null)
		{
			logger.warn(new MessageFormatMessage("Cannot add TOperationalCollabLink if any of TOperationalComponent {0}, TOperationalObject {1}, or TCollabLink {2} is null.", new Object[]{comp == null ? "NULL" : comp.getId(), obj == null ? "NULL" : obj.getId(), wireType == null ? "NULL" : wireType.getId()}));
			return null;
		}			
		TOperationalCollabLink link = getOperationalLinkForPairAndType(new ImmutableTriple<TOperationalObject, THADLarchElement, TCollabLink>(obj, comp, wireType));
		if (link == null)
			return addLink(wireType, getOrCreateAction(comp, wireType), getOrCreateAction(obj, wireType), obj, comp, TOperationalState.DESCRIBED_EXISTING);
		else
		{
			if (link.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || // we want the link to exist, there it is 
					link.getState().equals(TOperationalState.DESCRIBED_NONEXISTING) || // we confirmed it as non existing before, now its back 
					link.getState().equals(TOperationalState.PRESCRIBED_EXISTING_DENIED) // initially denied, now it does exist for whatever reasons
					)
				link.setState(TOperationalState.DESCRIBED_EXISTING);	
		}
		return link; 
	}
	
	
	
	private TOperationalCollabLink addLink(TCollabLink wireType, TAction collabAction, TAction objAction, TOperationalObject obj, THADLarchElement compOrConn, TOperationalState state)
	{
		TOperationalCollabLink link = new ObjectFactory().createTOperationalCollabLink();
		link.setId(wireType.getId()+"-"+UUID.randomUUID().toString());
		link.setState(state);
		link.setInstanceOf(mtu.lookupTypeRef(wireType));
		link.setCollabActionEndpoint(collabAction);
		link.setObjActionEndpoint(objAction);			
		super.getLink().add(link);
		linkIndex.put(new ImmutableTriple<TOperationalObject, THADLarchElement, TCollabLink>(obj, compOrConn, wireType), link);
		return link;
	}
			
	public TOperationalCollabLink removeLink(TOperationalCollabLink link)
	{
		if (link.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || link.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			link.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return link;
	}
	
	private TAction getOrCreateAction(TOperationalComponent el, TCollabLink wireType)
	{
		String aId = wireType.getCollabActionEndpoint().getId();
		TOperationalAction returnAction = null;
		for (TOperationalAction action : el.getOpAction())
		{
			if (action.getInstanceOf().getId().equals(aId))
			{				
				returnAction = action;
			}
		}
		if (returnAction == null)
		{
			returnAction = e2o.getOperationalCopy(wireType.getCollabActionEndpoint());		
			el.getAction().add(returnAction);
			el.getOpAction().add(returnAction);
		}
		if (!action2CompParent.containsKey(returnAction))
			action2CompParent.put(returnAction, el);
		return returnAction;
	}
	
	private TAction getOrCreateAction(TOperationalConnector el, TCollabLink wireType)
	{
		String aId = wireType.getCollabActionEndpoint().getId();
		TOperationalAction returnAction = null;
		for (TOperationalAction action : el.getOpAction())
		{
			if (action.getInstanceOf().getId().equals(aId))
			{				
				returnAction = action;
			}
		}
		if (returnAction == null)
		{
			returnAction = e2o.getOperationalCopy(wireType.getCollabActionEndpoint());		
			el.getAction().add(returnAction);
			el.getOpAction().add(returnAction);
		}
		if (!action2ConnParent.containsKey(returnAction))
			action2ConnParent.put(returnAction, el);
		return returnAction;
	}
	
	private TAction getOrCreateAction(TOperationalObject el, TCollabLink wireType)
	{
		String aId = wireType.getObjActionEndpoint().getId();
		TOperationalAction returnAction = null;
		for (TOperationalAction action : el.getOpAction())
		{
			if (action.getInstanceOf().getId().equals(aId))
			{				
				returnAction = action;
			}
		}
		if (returnAction == null)
		{
			returnAction = e2o.getOperationalCopy(wireType.getObjActionEndpoint());		
			el.getAction().add(returnAction);
			el.getOpAction().add(returnAction);
		}
		if (!action2ObjParent.containsKey(returnAction))
			action2ObjParent.put(returnAction, el);
		return returnAction;
	}
	
	public TOperationalComponent removeComponent(TOperationalComponent comp)
	{
		if (comp.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || comp.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			comp.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return comp;
	}
	
	public TOperationalConnector removeConnector(TOperationalConnector conn)
	{
		if (conn.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || conn.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			conn.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return conn;
	}
	
	public TOperationalObject removeObject(TOperationalObject obj)
	{
		if (obj.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || obj.getState().equals(TOperationalState.DESCRIBED_EXISTING))
			obj.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return obj;
	}	
	
	// returns a readonly list!
	public List<TOperationalObjectRef> getObjectRef() {
		return Collections.unmodifiableList(super.getObjectRef());
	}

	public TOperationalObjectRef addObjectRef(TOperationalObject fromObj, TOperationalObject toObj, TObjectRef refType, boolean isPrescribed)
	{
		if (elements.containsKey(fromObj.getId()) && elements.containsKey(toObj.getId()))
		{
			TOperationalObjectRef ref = new ObjectFactory().createTOperationalObjectRef();
			ref.setId(refType.getId()+"-"+UUID.randomUUID().toString());
			if (isPrescribed)
				ref.setState(TOperationalState.PRESCRIBED_EXISTING);
			else
				ref.setState(TOperationalState.DESCRIBED_EXISTING);
			ref.setInstanceOf(mtu.lookupTypeRef(refType));
			ref.setObjFrom(fromObj);
			ref.setObjTo(toObj);
			objRefIndex.put(new ImmutableTriple<THADLarchElement, THADLarchElement, TObjectRef>(fromObj,toObj, refType), ref);
			super.getObjectRef().add(ref);
			return ref;
		}
		return null;
	}
	
	public TOperationalObjectRef removeObjectRef(Triple<THADLarchElement,THADLarchElement, TObjectRef> spec)
	{
		TOperationalObjectRef ref = objRefIndex.get(spec);
		if (ref != null && (ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || ref.getState().equals(TOperationalState.DESCRIBED_EXISTING)))
			ref.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return ref;
	}
	
	public TOperationalObjectRef setObjectRefState(Triple<THADLarchElement, THADLarchElement, TObjectRef> spec, TOperationalState state)
	{
		TOperationalObjectRef ref = objRefIndex.get(spec);
		if (ref != null)
		{
			//TODO introduce state transition checker to see whether we really can set this state, use with care
			ref.setState(state);
		}
		return ref;
	}
	
	public TOperationalObjectRef addObjectRefIfNotExisting(TOperationalObject fromObj, TOperationalObject toObj, TObjectRef refType, boolean isPrescribed)
	{		
		if (fromObj == null || toObj == null || refType == null) 
		{
			logger.warn(new MessageFormatMessage("Cannot add TOperationalObjectRef if any of TOperationalObject from {0}, TOperationalObject to {1}, or TObjectRef {2} is null.", new Object[]{fromObj == null ? "NULL" : fromObj.getId(), toObj == null ? "NULL" : toObj.getId(), refType == null ? "NULL" : refType.getId()}));
			return null;
		}
		TOperationalObjectRef ref = objRefIndex.get(new ImmutableTriple<THADLarchElement,THADLarchElement, TObjectRef>(fromObj,  toObj,  refType));
		if (ref == null)
		{
			return this.addObjectRef(fromObj, toObj, refType, isPrescribed);
		}
		else
		{
			if (ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || // we want the ref to exist, there it is 
					ref.getState().equals(TOperationalState.DESCRIBED_NONEXISTING) || // we confirmed it as non existing before, now its back 
					ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING_DENIED) // initially denied, now it does exist for whatever reasons
					)
				ref.setState(TOperationalState.DESCRIBED_EXISTING);			
			if (ref.getState().equals(TOperationalState.PRESCRIBED_NONEXISTING)) // still there, tus set to denied as we just sensed it, if the surrogate is late, it will reset it to either described non existing or keep as denied
				ref.setState(TOperationalState.PRESCRIBED_NONEXISTING_DENIED);
		}
		return ref;
	}
	
	public TOperationalCollabRef addCollabRefIfNotExisting(TCollaborator fromCompConn, TCollaborator toCompConn, TCollabRef refType, boolean isPrescribed)
	{
		if (fromCompConn == null || toCompConn == null || refType == null) 
		{
			logger.warn(new MessageFormatMessage("Cannot add TOperationalObjectRef if any of TCollaborator from {0}, TCollaborator to {1}, or TCollabRef {2} is null.", new Object[]{fromCompConn == null ? "NULL" : fromCompConn.getId(), toCompConn == null ? "NULL" : toCompConn.getId(), refType == null ? "NULL" : refType.getId()}));
			return null;
		}
		TOperationalCollabRef ref = collabRefIndex.get(new ImmutableTriple<THADLarchElement,THADLarchElement, TCollabRef>(fromCompConn,  toCompConn,  refType));
		if (ref == null)
		{
			return this.addCollabRef(fromCompConn, toCompConn, refType, isPrescribed);
		}
		else
		{
			if (ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || // we want the ref to exist, there it is 
					ref.getState().equals(TOperationalState.DESCRIBED_NONEXISTING) || // we confirmed it as non existing before, now its back 
					ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING_DENIED) // initially denied, now it does exist for whatever reasons
					)
				ref.setState(TOperationalState.DESCRIBED_EXISTING);		
			if (ref.getState().equals(TOperationalState.PRESCRIBED_NONEXISTING)) // still there, tus set to denied as we just sensed it, if the surrogate is late, it will reset it to either described non existing or keep as denied
				ref.setState(TOperationalState.PRESCRIBED_NONEXISTING_DENIED);
		}
		return ref;
	}
	
	
	// returns a readonly list!
	public List<TOperationalCollabRef> getCollabRef() {
		return Collections.unmodifiableList(super.getCollabRef());
	}

	public TOperationalCollabRef addCollabRef(TCollaborator fromCompConn, TCollaborator toCompConn, TCollabRef refType, boolean isPrescribed)
	{
		if (elements.containsKey(fromCompConn.getId()) && elements.containsKey(toCompConn.getId()))
		{
			TOperationalCollabRef ref = new ObjectFactory().createTOperationalCollabRef();
			ref.setId(refType.getId()+"-"+UUID.randomUUID().toString());
			if (isPrescribed)
				ref.setState(TOperationalState.PRESCRIBED_EXISTING);
			else
				ref.setState(TOperationalState.DESCRIBED_EXISTING);
			ref.setInstanceOf(mtu.lookupTypeRef(refType));
			ref.setCompconnFrom(fromCompConn);
			ref.setCompconnTo(toCompConn);
			collabRefIndex.put(new ImmutableTriple<THADLarchElement, THADLarchElement, TCollabRef>(fromCompConn,toCompConn, refType), ref);
			super.getCollabRef().add(ref);
			return ref;
		}
		return null;
	}
	
	public TOperationalCollabRef removeCollabRef(Triple<THADLarchElement, THADLarchElement, TCollabRef> spec)
	{
		TOperationalCollabRef ref = collabRefIndex.get(spec);
		if (ref != null && (ref.getState().equals(TOperationalState.PRESCRIBED_EXISTING) || ref.getState().equals(TOperationalState.DESCRIBED_EXISTING)))
			ref.setState(TOperationalState.PRESCRIBED_NONEXISTING);
		return ref;
	}
	
	public TOperationalCollabRef setCollabRefState(Triple<THADLarchElement, THADLarchElement, TCollabRef> spec, TOperationalState state)
	{
		TOperationalCollabRef ref = collabRefIndex.get(spec);
		if (ref != null)
		{
			//TODO introduce state transition checker to see whether we really can set this state, use with care
			ref.setState(state);
		}
		return ref;
	}
	
	
	public List<TActivityScopeInstance> getActivityScope() {
		return super.getActivityScope();
	}

	public HADLruntimeModel()
	{
		super();
	}
	
	
	private Hashtable<Triple<TOperationalObject,THADLarchElement, TCollabLink>,TOperationalCollabLink> linkIndex = new Hashtable<Triple<TOperationalObject,THADLarchElement, TCollabLink>, TOperationalCollabLink>();
	private Hashtable<Triple<THADLarchElement,THADLarchElement, TCollabRef>,TOperationalCollabRef> collabRefIndex = new Hashtable<Triple<THADLarchElement,THADLarchElement, TCollabRef>, TOperationalCollabRef>();
	private Hashtable<Triple<THADLarchElement,THADLarchElement, TObjectRef>,TOperationalObjectRef> objRefIndex = new Hashtable<Triple<THADLarchElement,THADLarchElement, TObjectRef>, TOperationalObjectRef>();
	private Hashtable<TOperationalAction,TOperationalObject> action2ObjParent = new Hashtable<TOperationalAction,TOperationalObject>();
	private Hashtable<TOperationalAction,TOperationalComponent> action2CompParent = new Hashtable<TOperationalAction,TOperationalComponent>();
	private Hashtable<TOperationalAction,TOperationalConnector> action2ConnParent = new Hashtable<TOperationalAction,TOperationalConnector>();
	private Hashtable<TTypeRef,List<TOperationalComponent>> hADLType2compInst = new Hashtable<TTypeRef,List<TOperationalComponent>>();
	private Hashtable<TTypeRef,List<TOperationalConnector>> hADLType2connInst = new Hashtable<TTypeRef,List<TOperationalConnector>>();
	private Hashtable<TTypeRef,List<TOperationalObject>> hADLType2objInst = new Hashtable<TTypeRef,List<TOperationalObject>>();

	public List<TOperationalComponent> getCompInstancesOfType(TTypeRef ref)
	{
		if (ref != null && hADLType2compInst.containsKey(ref))
			return new ArrayList<TOperationalComponent>(hADLType2compInst.get(ref));
		else
			return new ArrayList<TOperationalComponent>();
	}
	
	public List<TOperationalConnector> getConnInstancesOfType(TTypeRef ref)
	{
		if (ref != null && hADLType2connInst.containsKey(ref))
			return new ArrayList<TOperationalConnector>(hADLType2connInst.get(ref));
		else
			return new ArrayList<TOperationalConnector>();
	}
	
	public List<TOperationalObject> getObjInstancesOfType(TTypeRef ref)
	{
		if (ref != null && hADLType2objInst.containsKey(ref))
			return new ArrayList<TOperationalObject>(hADLType2objInst.get(ref));
		else
			return new ArrayList<TOperationalObject>();
	}			
	
	public TOperationalCollabLink getOperationalLinkForPairAndType(Triple<TOperationalObject, THADLarchElement, TCollabLink> spec)
	{
		return linkIndex.get(spec);
	}
	
	public Triple<TOperationalObject,TOperationalComponent,TOperationalConnector> getOperationalPairForLink(TCollabLink link)
	{
		if (super.getLink().contains(link))
		{
			TAction cA = link.getObjActionEndpoint();
			TOperationalObject obj = action2ObjParent.get(cA);
			
			TAction oA = link.getCollabActionEndpoint();
			TOperationalComponent oComp = action2CompParent.get(oA);
			TOperationalConnector oConn = action2ConnParent.get(oA);
			
			if (obj == null || (oComp == null && oConn == null))
			{
				logger.warn(new MessageFormatMessage("Inconsistent Data: Having TCollabLink {0} but cannot resolve Endpoints: TCollaborator {2} and/or TCollabObject {1} are null.", new Object[]{link, obj == null ? "NULL" : obj.getId(), oComp == null && oConn == null ? "NULL" : (oComp == null ? oConn.getId() : oComp.getId())}));
				return null; // should never happen ;-)
			}
			else
			{
				return new ImmutableTriple<TOperationalObject,TOperationalComponent,TOperationalConnector>(obj, oComp, oConn);
			}								
		}
		return null; //might happen
	}
	

	
	public List<Entry<TResourceDescriptor,TOperationalComponent>> matchOperationalComponentOnResourceDescriptor(TTypeRef ofType, Set<TResourceDescriptor> rds)
	{		
		List<Entry<TResourceDescriptor,TOperationalComponent>> matches = new ArrayList<Entry<TResourceDescriptor,TOperationalComponent>>();
		if (hADLType2compInst.containsKey(ofType))
		{
			for (TResourceDescriptor rd : rds)
			{
				for (TOperationalComponent oc : hADLType2compInst.get(ofType))
				{
					for (TResourceDescriptor rdBaseSet : oc.getResourceDescriptor())
					{
						if (rd.equals(rdBaseSet))
						{
							matches.add(new AbstractMap.SimpleEntry<TResourceDescriptor,TOperationalComponent>(rd,oc));
							break; // don't check the other resource descriptors of this element 
						}
					}
				}
			}
		}
		return matches;		
	}
	
	public List<Entry<TResourceDescriptor,TOperationalConnector>> matchOperationalConnectorOnResourceDescriptor(TTypeRef ofType, Set<TResourceDescriptor> rds)
	{
		List<Entry<TResourceDescriptor,TOperationalConnector>> matches = new ArrayList<Entry<TResourceDescriptor,TOperationalConnector>>();
		if (hADLType2connInst.containsKey(ofType))
		{
			for (TResourceDescriptor rd : rds)
			{
				for (TOperationalConnector oc : hADLType2connInst.get(ofType))
				{
					for (TResourceDescriptor rdBaseSet : oc.getResourceDescriptor())
					{
						if (rd.equals(rdBaseSet))
						{
							matches.add(new AbstractMap.SimpleEntry<TResourceDescriptor,TOperationalConnector>(rd,oc));
							break; // don't check the other resource descriptors of this element 
						}
					}
				}
			}
		}
		return matches;		
	}
	
	public List<Entry<TResourceDescriptor,TOperationalObject>> matchOperationalObjectOnResourceDescriptor(TTypeRef ofType, Set<TResourceDescriptor> rds)
	{
		List<Entry<TResourceDescriptor,TOperationalObject>> matches = new ArrayList<Entry<TResourceDescriptor,TOperationalObject>>();
		if (hADLType2objInst.containsKey(ofType))
		{
			for (TResourceDescriptor rd : rds)
			{
				for (TOperationalObject oc : hADLType2objInst.get(ofType))
				{
					for (TResourceDescriptor rdBaseSet : oc.getResourceDescriptor())
					{
						if (rd.equals(rdBaseSet))
						{
							matches.add(new AbstractMap.SimpleEntry<TResourceDescriptor,TOperationalObject>(rd,oc));
							break; // don't check the other resource descriptors of this element 
						}
					}
				}
			}
		}
		return matches;		
	}
}
