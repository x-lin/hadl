package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import javax.xml.bind.JAXBElement;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement.Extension;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutableRefExtension;
import at.ac.tuwien.dsg.hadl.schema.executable.TSurrogate;

public class DefaultSurrogateFactory implements SurrogateFactory {

	Creator<ISurrogate> genericSurr = new Creator<ISurrogate>();
	Creator<ICollaboratorSurrogate> collabSurr = new Creator<ICollaboratorSurrogate>();
	Creator<IObjectSurrogate> objectSurr = new Creator<IObjectSurrogate>();
	
	@Override
	public ISurrogate getInstance(THADLarchElement type) throws InsufficientModelInformationException {
		String fqn = getFQN(type); 
		return genericSurr.getInstance(fqn, ISurrogate.class);
	}

	@Override
	public ICollaboratorSurrogate getInstance(TCollaborator collaboratorSpec) throws InsufficientModelInformationException {
		String fqn = getFQN(collaboratorSpec); 
		return collabSurr.getInstance(fqn, ICollaboratorSurrogate.class);
	}

	@Override
	public IObjectSurrogate getInstance(TCollabObject objectSpec) throws InsufficientModelInformationException {
		String fqn = getFQN(objectSpec); 
		return objectSurr.getInstance(fqn, IObjectSurrogate.class);
	}
	
	@SuppressWarnings("rawtypes")
	private String getFQN(THADLarchElement el) throws InsufficientModelInformationException
	{
		//Set<String> fqns = new HashSet<String>();
		for (Extension ext : el.getExtension())
		{
			Object obj2 = ext.getAny();

			if (obj2 instanceof JAXBElement)
				obj2 = ((JAXBElement)obj2).getValue();					
			if (obj2 instanceof TExecutableRefExtension)
			{									
				for (Object obj : ((TExecutableRefExtension)obj2).getExecutableViaSurrogate())
				{
					TSurrogate surr = null;
//					if (obj instanceof JAXBElement)
//					{
//						surr = ((JAXBElement<TSurrogate>)obj).getValue();
//					}
//					else 
					if (obj instanceof TSurrogate)
						surr = (TSurrogate)obj;
					if (surr.getSurrogateFQN() != null)
						return surr.getSurrogateFQN();
					//fqns.add(surr.getSurrogateFQN());
				}					
			}			
		}
		// just return one TODO: return all found FQNs including their supported platform
		//if (!fqns.isEmpty())
		//	return fqns.iterator().next();
		//else
		throw new InsufficientModelInformationException("Found no Extension on ExecutableRef", TExecutableRefExtension.class.getCanonicalName());		
	}
	
	private static class Creator<T extends ISurrogate>
	{

		@SuppressWarnings("unchecked")
		private T getInstance(String fqn, Class<T> tClass)
		{
			if (fqn == null || tClass == null)
				return null;
			try {
				Class<?> clazz = Class.forName(fqn);
				if (tClass.isAssignableFrom(clazz))
				{
					T surr = (T)clazz.newInstance();					
					return surr;
				}
				else 
					return null;
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {			
				e.printStackTrace();
				return null;
			}
		}
	}

}
