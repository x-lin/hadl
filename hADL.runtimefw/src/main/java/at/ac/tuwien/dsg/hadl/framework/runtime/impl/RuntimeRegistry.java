package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;



public class RuntimeRegistry 
{
		
	private Hashtable<TSurrogateInstanceRef, ISurrogate> surrReg = new Hashtable<TSurrogateInstanceRef, ISurrogate>();
	private Hashtable<TResourceDescriptor,TSurrogateInstanceRef> crossIndex = new Hashtable<TResourceDescriptor, TSurrogateInstanceRef>();
	
	public TSurrogateInstanceRef registerSurrogate(ISurrogate surr, TResourceDescriptor rd) 
	{		
		String id = surr.getClass().getSimpleName()+UUID.randomUUID().toString();		
		//TODO: LATER make id that can serve as HREF eventually
		TSurrogateInstanceRef ref = new ObjectFactory().createTSurrogateInstanceRef();
		ref.setHref(id);		
		surrReg.put(ref,  surr);	
		crossIndex.put(rd, ref);
		return ref;
	}
	
	public ICollaboratorSurrogate getCollaboratorSurrogate(TSurrogateInstanceRef ref)
	{		
		if (surrReg.containsKey(ref))
		{
			ISurrogate surr = surrReg.get(ref);
			if (surr instanceof ICollaboratorSurrogate)
				return (ICollaboratorSurrogate) surr;
			else
				return null;
		}
		return null;
	}
	
	public IObjectSurrogate getObjectSurrogate(TSurrogateInstanceRef ref)
	{
		if (surrReg.containsKey(ref))
		{
			ISurrogate surr = surrReg.get(ref);
			if (surr instanceof IObjectSurrogate)
				return (IObjectSurrogate) surr;
			else
				return null;
		}
		return null;
	}
	
	public Collection<ISurrogate> getAllSurrogates()
	{
		return Collections.unmodifiableCollection(surrReg.values());				
	}

	
	
	
}
