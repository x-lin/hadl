package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory;



public class Executable2OperationalTransformer
{
//	private static JAXBContext jc;
//	private static Marshaller actM;
//	private static Unmarshaller actUM;
	
	public Executable2OperationalTransformer()
	{
			
	}

//	public THADLarchElement toOperationalCopy(THADLarchElement fromExecutable)
//	{
//		// create a copy of the original as an operational element (deep copy at the level of xsd --> idrefs are the edges)
//		
//		return null;
//	}
	
	
	public TOperationalComponent toOperationalCopy(THumanComponent fromExecutable)
	{
		// create a copy of the original as an operational element (deep copy at the level of xsd --> idrefs are the edges)
		TOperationalComponent oc = new ObjectFactory().createTOperationalComponent();
		copyNameAndId(oc, fromExecutable);		
		copyActions(oc.getOpAction(), fromExecutable.getAction(), oc.getAction());
		return oc;
	}
	
	public TOperationalConnector toOperationalCopy(TCollabConnector fromExecutable)
	{
		// create a copy of the original as an operational element (deep copy at the level of xsd --> idrefs are the edges)
		TOperationalConnector oc = new ObjectFactory().createTOperationalConnector();
		copyNameAndId(oc, fromExecutable);		
		copyActions(oc.getOpAction(), fromExecutable.getAction(), oc.getAction());
		return oc;
	}
	
	public TOperationalObject toOperationalCopy(TCollabObject fromExecutable)
	{
		// create a copy of the original as an operational element (deep copy at the level of xsd --> idrefs are the edges)
		TOperationalObject oo = new ObjectFactory().createTOperationalObject();
		copyNameAndId(oo, fromExecutable);		
		copyActions(oo.getOpAction(), fromExecutable.getAction(), oo.getAction());		
		return oo;		
	}
	
	private void copyNameAndId(THADLarchElement newEl, THADLarchElement prototype)
	{
		if (prototype.getName() != null)
			newEl.setName("Instance of "+prototype.getName());
		newEl.setId(prototype.getId()+"-"+UUID.randomUUID().toString());		
	}
	
	private void copyActions(List<TOperationalAction> newActions, List<TAction> copyFrom, List<TAction> allActions)
	{
		for(TAction action : copyFrom)
		{			
			TOperationalAction oa = getOperationalCopy(action);
			newActions.add(oa); // corresponds in XML to the idrefs which capture what actions are operational actions (not just plain actions)
			allActions.add(oa);			
		}
	}
	
	public TOperationalAction getOperationalCopy(TAction original)
	{
		// TODO complete, only ID, name, and typeRef is set for now
		TOperationalAction action = new at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory().createTOperationalAction();				
		copyNameAndId(action, original);		
		action.setInstanceOf(original);		
		return action;
	}
}
