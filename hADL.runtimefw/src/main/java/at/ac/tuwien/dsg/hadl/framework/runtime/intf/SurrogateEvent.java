package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;



public class SurrogateEvent 
{
	private TSurrogateInstanceRef source;
	private ESurrogateStatus status;
	private SurrogateException optEx = null;
	private THADLarchElement operationalElement = null;
		
	public SurrogateException getOptionalEx() {
		return optEx;
	}
	public void setOptionalEx(SurrogateException optEx) {
		this.optEx = optEx;
	}
	public TSurrogateInstanceRef getSource() {
		return source;
	}
	public void setSource(TSurrogateInstanceRef source) {
		this.source = source;
	}
	public ESurrogateStatus getStatus() {
		return status;
	}
	public void setStatus(ESurrogateStatus status) {
		this.status = status;
	}
	public THADLarchElement getOperationalElement() {
		return operationalElement;
	}
	public void setOperationalElement(THADLarchElement operationalElement) {
		this.operationalElement = operationalElement;
	}
	
	public SurrogateEvent(TSurrogateInstanceRef source, ESurrogateStatus status) {
		super();
		this.source = source;
		this.status = status;
	}
	public SurrogateEvent(TSurrogateInstanceRef source, ESurrogateStatus status,
			SurrogateException optEx) {
		super();
		this.source = source;
		this.status = status;
		this.optEx = optEx;
	}
	
	public SurrogateEvent(TSurrogateInstanceRef source,
			ESurrogateStatus status, SurrogateException optEx,
			THADLarchElement operationalElement) {
		super();
		this.source = source;
		this.status = status;
		this.optEx = optEx;
		this.operationalElement = operationalElement;
	}
	
	public SurrogateEvent(TSurrogateInstanceRef source,
			ESurrogateStatus status, THADLarchElement operationalElement) {
		super();
		this.source = source;
		this.status = status;
		this.operationalElement = operationalElement;
	}

	
	
	
	
}
