package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import rx.Observable;

public interface IObjectSurrogate extends ISurrogate {
	
	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalObject surrogateFor);	
	
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalConnector oppositeElement, TAction oppositeAction, TOperationalCollabLink link);
	
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalComponent oppositeElement, TAction oppositeAction, TOperationalCollabLink link);
	
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalComponent oppositeElement, TOperationalCollabLink link);
	
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalConnector oppositeElement, TOperationalCollabLink link);

	public Observable<SurrogateEvent> relating(TOperationalObject oppositeElement, TObjectRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalObjectRef ref);
	
}
