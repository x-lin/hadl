package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;


public class SurrogateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5287156050210740034L;

	private TSurrogateInstanceRef surr = null;

	public SurrogateException(TSurrogateInstanceRef surr, String msg, Throwable cause) {
		super(msg, cause);
		this.surr = surr;
	}

	public TSurrogateInstanceRef getSurr() {
		return surr;
	}
	
	
	
}
