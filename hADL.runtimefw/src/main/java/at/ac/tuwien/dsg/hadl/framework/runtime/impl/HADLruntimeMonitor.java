package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.apache.logging.log4j.message.MessageFormatMessage;
import org.apache.logging.log4j.message.SimpleMessage;

import rx.Observable;
import rx.Observer;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeCollaboratorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeObjectEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ICollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.IObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.MultiFactoryException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateFactory;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabConnector;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THumanComponent;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.core.TSubstructure;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalAction;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TTypeRef;

public class HADLruntimeMonitor
{	
	@Inject private SurrogateFactoryResolver sfr; 
	@Inject private Executable2OperationalTransformer e2o; 
	@Inject private RuntimeRegistry reg; 
	@Inject private HADLruntimeModel model;
	@Inject private ModelTypesUtil mtu; 
	@Inject private SensorFactory factory;
	protected TActivityScope scope;

	protected static final Logger logger = LogManager.getLogger(HADLruntimeMonitor.class);

	public HADLruntimeMonitor()
	{

	}

	public void init(TActivityScope scope)
	{
		this.scope = scope;
	}

	// REQUEST PART
	public Observable<LoadEvent> loadFrom(TOperationalObject oo, SensingScope sensingScope)
	{		
		// simple approach for now: assume sensors are created anew each time and discarded thereafter		
		try {
			THADLarchElement elType = mtu.getByTypeRef(oo.getInstanceOf());
			ICollabObjectSensor sensor = factory.getInstance((TCollabObject)elType); // cast exception would indicate serious data inconsistency 
			if (sensor != null)
			{
				sensor.acquire(this.scope, oo);
				// further logic to wait until setup completed
				Observable<LoadEvent> stream = sensor.loadOnce(sensingScope);
				DiffingLoadEventProcessor proc = new DiffingLoadEventProcessor();
				return stream.doOnNext(proc).doOnTerminate(proc);
			}
			else
				logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalObject {0} due to no sensor returned from Factory", oo.getId()));
		} catch (InsufficientModelInformationException e) {
			logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalObject {0} due to error while obtaining sensor from Factory", oo.getId()), e);
			return Observable.error(e);
		}	
		return Observable.empty();
	}

	public Observable<LoadEvent> loadFrom(TOperationalComponent oo, SensingScope sensingScope)
	{		
		// simple approach for now: assume sensors are created anew each time and discarded thereafter		
		try {
			THADLarchElement elType = mtu.getByTypeRef(oo.getInstanceOf());
			ICollaboratorSensor sensor = factory.getInstance((TCollaborator)elType); // cast exception would indicate serious data inconsistency 
			if (sensor != null)
			{
				sensor.acquire(this.scope, oo);
				// further logic to wait until setup completed
				Observable<LoadEvent> stream = sensor.loadOnce(sensingScope);
				DiffingLoadEventProcessor proc = new DiffingLoadEventProcessor();
				return stream.doOnNext(proc).doOnTerminate(proc);
			}
			else
				logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalComponent {0} due to no sensor returned from Factory", oo.getId()));
		} catch (InsufficientModelInformationException e) {			
			logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalComponent {0} due to error while obtaining sensor from Factory", oo.getId()), e);
			return Observable.error(e);
		}	
		return Observable.empty();
	}

	public Observable<LoadEvent> loadFrom(TOperationalConnector oo, SensingScope sensingScope)
	{		
		// simple approach for now: assume sensors are created anew each time and discarded thereafter		
		try {
			THADLarchElement elType = mtu.getByTypeRef(oo.getInstanceOf());
			ICollaboratorSensor sensor = factory.getInstance((TCollaborator)elType); // cast exception would indicate serious data inconsistency 
			if (sensor != null)
			{
				sensor.acquire(this.scope, oo);
				// further logic to wait until setup completed
				Observable<LoadEvent> stream = sensor.loadOnce(sensingScope);
				DiffingLoadEventProcessor proc = new DiffingLoadEventProcessor();
				return stream.doOnNext(proc).doOnTerminate(proc);					 				
			}
			else
				logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalConnector {0} due to no sensor returned from Factory", oo.getId()));
		} catch (InsufficientModelInformationException e) {						
			logger.warn(new MessageFormatMessage("Cannot handle Load request on TOperationalConnector {0} due to error while obtaining sensor from Factory", oo.getId()), e);
			return Observable.error(e);
		}	
		return Observable.empty();
	}

	public Observable<LoadEvent> loadFromAllInScopeOfType(THADLarchElement elType, SensingScope sensingScope)
	{		
		TTypeRef typeRef = mtu.lookupTypeRef(elType);		
		if (typeRef != null)			
		{
			List<Observable<LoadEvent>> observables = new ArrayList<Observable<LoadEvent>>();			
			for (TOperationalComponent oc : model.getCompInstancesOfType(typeRef))
			{
				observables.add(loadFrom(oc, sensingScope));				
			}
			for (TOperationalConnector oc : model.getConnInstancesOfType(typeRef))
			{
				observables.add(loadFrom(oc, sensingScope));
			}
			for (TOperationalObject oc : model.getObjInstancesOfType(typeRef))
			{
				observables.add(loadFrom(oc, sensingScope));
			}			
			Observable<LoadEvent> stream = Observable.merge(observables);
			 // locally event processing triggered in the individual loadFrom methods
			return stream;
		}
		else
			logger.warn(new FormattedMessage("Cannot resolve supplied Element {0} to type reference via ModelTypesUtil; ignoring load request", elType.getId()));
		return Observable.empty();
	}


	// PROCESSING PART

	public void addLoadSensorSource(Observable<LoadEvent> eventStream)
	{
		final DiffingLoadEventProcessor proc = new DiffingLoadEventProcessor();
		eventStream.subscribe(proc, new Action1<Throwable>() {@Override
		public void call(Throwable t1) {			
			proc.call(); // complete diffing process
			logger.warn(new MessageFormatMessage("Terminating LoadObserver due to obtained exeption", t1));			
		}
		}, proc);
	}


	private class DiffingLoadEventProcessor implements Action1<LoadEvent>, Action0
	{
		private HashSet<THADLarchElement> createdOperationalElementsInSession = new HashSet<THADLarchElement>();
		private HashSet<THADLarchElement> processedElementsInSession = new HashSet<THADLarchElement>();
		//private HashSet<TOperationalObjectRef> sensedObjectRefs = new HashSet<TOperationalObjectRef>();
		private Hashtable<String, SensingScope> relationsFromOrigin = new Hashtable<String, SensingScope>();
						
		@Override
		public void call(LoadEvent t1) {			
			// add event data to model:	
			// DONE for each event, we need to check whether the operational elements is already present (by checking available TResourceDescriptors)
			// then for each inserted element we need to insert also the relation (link or reference) through which element has been loaded
			// alternative for an existing element we need to check whether such a relation exists, and if not, insert it.

			//FIXME: what to do with user removed links and elements? single event is not guaranteed to be complete (sensing results may be split across multiple events)  
			// --> Do a diff, but that again might not have full data available 
			// --> maybe if we use the sensing scope as indicator what elements are sensed, Problem: not quite clear when sensing is done (maybe via sensor observable end)
			// 		--> Pseudo alg: for each sensor origin/root and sensing scope filtering the relations: 	1) tag subtree of all elements and links, 
			//																								2) see which are not touched after load process
			// 																								3) remove those elements
			// --> basically/much simpler: we need change detection sensors

			if (t1.getOriginId() != null && t1.getOriginId().length() > 0)
			{
				// capture all kind of relations that we might sense
				SensingScope overallConsideredScope = relationsFromOrigin.get(t1.getOriginId());
				if (overallConsideredScope == null)
				{
					overallConsideredScope = new SensingScope();
					relationsFromOrigin.put(t1.getOriginId(), overallConsideredScope);
				}
				overallConsideredScope.mergeWith(t1.getSensingScope());
			}
			
			// collect relations to explicitly identified destinations
			SensingScope checkedScope = t1.getSensingScope();
			SensingScope notFoundScope = new SensingScope(); // all relations that the sensor was capable of but didn't find (anymore)			
			// copy all refs from CheckedScope into notFoundScope that are not listed in the loadevent viaXXX elements 						
			notFoundScope.getLinks().addAll(checkedScope.getLinks());
			notFoundScope.getLinks().removeAll(t1.getLoadedViaLinkType());
			notFoundScope.getActions().addAll(checkedScope.getActions());
			notFoundScope.getActions().removeAll(t1.getLoadedViaOppositeActionType());
			notFoundScope.getSubstructureWires().addAll(checkedScope.getSubstructureWires());
			notFoundScope.getSubstructureWires().removeAll(t1.getLoadedViaSubstructureWireType());
			
			Set<String> preExistingOperationalElements = new HashSet<String>();
			if (t1 instanceof OperativeCollaboratorEvent)
			{
				OperativeCollaboratorEvent oe = (OperativeCollaboratorEvent)t1;				
				notFoundScope.getCollabRefs().addAll(checkedScope.getCollabRefs());
				notFoundScope.getCollabRefs().removeAll(oe.getLoadedViaRef());				
				process(oe, preExistingOperationalElements);					
			}
			else if (t1 instanceof OperativeObjectEvent)
			{
				OperativeObjectEvent oe = (OperativeObjectEvent)t1;
				notFoundScope.getObjectRefs().addAll(checkedScope.getObjectRefs());
				notFoundScope.getObjectRefs().removeAll(oe.getLoadedViaRef());				
				process(oe, preExistingOperationalElements);				
			}
			else
				logger.warn(new MessageFormatMessage("Ignoring unknown LoadEvent subclass: {0} from {1}", t1, t1.getOriginId()));

			// now remove relations that the sensor was able to check but not found for the elements identified
			removeRelations(t1.getOriginId(), preExistingOperationalElements, notFoundScope);			
		}
		
		@Override
		public void call() {
			// TODO diffing: what should be removed
			// remove relations to no longer connected destinations
			// computationally expensive to collect all relations of processed type (relationsFromOrigin.Values) but not considering new and processed elements
			// (rather easy to do with a graph database, rather tedious here --> we just do a full search, not very performent for larger sets but sufficient for now)
			for(String source : relationsFromOrigin.keySet())
			{
				Set<THADLarchElement> ignoreElements = new HashSet<THADLarchElement>();
				ignoreElements.addAll(processedElementsInSession);
				ignoreElements.addAll(createdOperationalElementsInSession);
				removeRelations(source, relationsFromOrigin.get(source), ignoreElements);
			}
		}				

		private void process(OperativeCollaboratorEvent e, Set<String> preExistingOperationalElements)
		{
			logger.trace(new MessageFormatMessage("LoadObserver processes OperativeCollaboratorEvent from Origin: {0}", e.getOriginId()));
			if (e.getCollaboratorType() != null)
			{					
				TTypeRef tRef = mtu.lookupTypeRef(e.getCollaboratorType());
				List<TResourceDescriptor> loadedRD = new ArrayList<TResourceDescriptor>(e.getDescriptors());
				TOperationalComponent oComp = e.getCompConnObjOrigin().getLeft();
				TOperationalConnector oConn = e.getCompConnObjOrigin().getMiddle();
				TOperationalObject oObj = e.getCompConnObjOrigin().getRight();


				if (tRef != null && e.getCollaboratorType() instanceof THumanComponent)
				{
					List<Entry<TResourceDescriptor,TOperationalComponent>> matches = model.matchOperationalComponentOnResourceDescriptor(tRef, e.getDescriptors());
					for (Entry<TResourceDescriptor,TOperationalComponent> match : matches)
					{
						if (loadedRD.contains(match.getKey()))	// for matches: check the relation to the loading/origin element
						{
							loadedRD.remove(match.getKey());		
							if (!createdOperationalElementsInSession.contains(match.getKey()))
							{ 
								preExistingOperationalElements.add(match.getValue().getId());
								processedElementsInSession.add(match.getValue());
							}
							wireCollaborator(e, oComp, oConn, oObj, match.getValue(), null);							
						}						
					}
					// for non-matches:
					if (!loadedRD.isEmpty())
					{
						List<SurrogateFactory> sfList = new ArrayList<SurrogateFactory>();
						try {
							sfList = sfr.resolveFactoryFrom(e.getCollaboratorType());
						} catch (MultiFactoryException e2) {
							e2.printStackTrace();
						}							 
						for (TResourceDescriptor rdNew : loadedRD)
						{
							// create Element:								
							TOperationalComponent oc = e2o.toOperationalCopy((THumanComponent)e.getCollaboratorType());
							oc.getResourceDescriptor().add(rdNew);
							oc.setState(TOperationalState.DESCRIBED_EXISTING);				
							oc.setInstanceOf(mtu.lookupTypeRef(e.getCollaboratorType()));
							model.addComponent(oc);
							createdOperationalElementsInSession.add(oc); //capture the fact that this element was created within sensing session (observable) 
							// wire element
							wireCollaborator(e, oComp, oConn, oObj, oc, null);
							// check for surrogate (might not be available for observed/loaded elements, might be limited to sensor role)
							if (!sfList.isEmpty())
							{
								try {
									ICollaboratorSurrogate surr = sfList.get(0).getInstance(e.getCollaboratorType());				
									TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rdNew);
									surr.setSurrogateInstanceRef(ref);
									oc.setOperationalViaSurrogate(ref);
									//observables.add(surr.acquire(scope, oc)); lazy acquiring in surrogate needs to be explicitly triggered by a client that want to enact something through the surrogate
								} catch (InsufficientModelInformationException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}																
						}
					}				
				}
				else if (tRef != null && e.getCollaboratorType() instanceof TCollabConnector)
				{
					List<Entry<TResourceDescriptor,TOperationalConnector>> matches = model.matchOperationalConnectorOnResourceDescriptor(tRef, e.getDescriptors());
					for (Entry<TResourceDescriptor,TOperationalConnector> match : matches)
					{
						if (loadedRD.contains(match.getKey()))	// for matches: check the relation to the loading/origin element
						{
							loadedRD.remove(match.getKey());	
							if (!createdOperationalElementsInSession.contains(match.getKey()))
							{ 
								preExistingOperationalElements.add(match.getValue().getId());
								processedElementsInSession.add(match.getValue());
							}
							wireCollaborator(e, oComp, oConn, oObj, null, match.getValue());
						}						
					}
					// for non-matches:
					if (!loadedRD.isEmpty())
					{
						List<SurrogateFactory> sfList = new ArrayList<SurrogateFactory>();
						try {
							sfList = sfr.resolveFactoryFrom(e.getCollaboratorType());
						} catch (MultiFactoryException e2) {
							e2.printStackTrace();
						}							 
						for (TResourceDescriptor rdNew : loadedRD)
						{
							// create Element:								
							TOperationalConnector oc = e2o.toOperationalCopy((TCollabConnector)e.getCollaboratorType());
							oc.getResourceDescriptor().add(rdNew);
							oc.setState(TOperationalState.DESCRIBED_EXISTING);				
							oc.setInstanceOf(mtu.lookupTypeRef(e.getCollaboratorType()));
							model.addConnector(oc);
							createdOperationalElementsInSession.add(oc); //capture the fact that this element was created within sensing session (observable) 
							// wire element
							wireCollaborator(e, oComp, oConn, oObj, null, oc);							
							// check for surrogate (might not be available for observed/loaded elements, might be limited to sensor role)
							if (!sfList.isEmpty())
							{
								try {
									ICollaboratorSurrogate surr = sfList.get(0).getInstance(e.getCollaboratorType());				
									TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rdNew);
									surr.setSurrogateInstanceRef(ref);
									oc.setOperationalViaSurrogate(ref);
									//observables.add(surr.acquire(scope, oc)); lazy acquiring in surrogate
								} catch (InsufficientModelInformationException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}																
						}
					}
				}	
				else
				{					
					logger.warn(new FormattedMessage("Cannot resolve supplied TCollaborator {0} to type reference via ModelTypesUtil; ignoring event", e.getCollaboratorType().getId()));	
				}
			}
			else 
			{				
				logger.warn(new SimpleMessage("CollaboratorType of loaded elements is NULL but required in OperativeCollaboratorEvent; ignoring event"));
				logger.debug(new FormattedMessage("Incomplete OperativeObjectEvent: {0}", e.toString()));	 
			}
		}


		private void process(OperativeObjectEvent e, Set<String> preExistingOperationalElements)
		{
			logger.trace(new MessageFormatMessage("LoadObserver processes OperativeObjectEvent from Origin: {0}{1}{2}", 
					new Object[]{e.getCompConnObjOrigin().getLeft() == null ? "":e.getCompConnObjOrigin().getLeft().getId(), 
							e.getCompConnObjOrigin().getMiddle() == null ? "":e.getCompConnObjOrigin().getMiddle().getId(),
									e.getCompConnObjOrigin().getRight() == null ? "":e.getCompConnObjOrigin().getRight().getId()}));
			if (e.getObjType() != null)
			{				
				TCollabObject tObj = e.getObjType();
				TTypeRef tRef = mtu.lookupTypeRef(tObj);
				List<TResourceDescriptor> loadedRD = new ArrayList<TResourceDescriptor>(e.getDescriptors());
				TOperationalComponent oComp = e.getCompConnObjOrigin().getLeft();
				TOperationalConnector oConn = e.getCompConnObjOrigin().getMiddle();
				TOperationalObject oObj = e.getCompConnObjOrigin().getRight();

				if (tRef != null)
				{
					List<Entry<TResourceDescriptor,TOperationalObject>> matches = model.matchOperationalObjectOnResourceDescriptor(tRef, e.getDescriptors());
					for (Entry<TResourceDescriptor,TOperationalObject> match : matches)
					{
						if (loadedRD.contains(match.getKey()))	
						{
							loadedRD.remove(match.getKey());								
							if (!createdOperationalElementsInSession.contains(match.getKey()))
							{ 
								preExistingOperationalElements.add(match.getValue().getId());
								processedElementsInSession.add(match.getValue());
							}
							// for matches: check the relation to the loading/origin element
							wireObject(e, oComp, oConn, oObj, match.getValue(), tObj);	
						}						
					}
					// for non-matches:
					if (!loadedRD.isEmpty())
					{
						List<SurrogateFactory> sfList = new ArrayList<SurrogateFactory>();
						try {
							sfList = sfr.resolveFactoryFrom(e.getObjType());
						} catch (MultiFactoryException e2) {
							e2.printStackTrace();
						}							 
						for (TResourceDescriptor rdNew : loadedRD)
						{
							// create Element:								
							TOperationalObject oc = e2o.toOperationalCopy(tObj);
							oc.getResourceDescriptor().add(rdNew);
							oc.setState(TOperationalState.DESCRIBED_EXISTING);				
							oc.setInstanceOf(tRef);
							model.addObject(oc);
							createdOperationalElementsInSession.add(oc); //capture the fact that this element was created within sensing session (observable) 
							// link up the elements:
							wireObject(e, oComp, oConn, oObj, oc, tObj);						

							// check for surrogate (might not be available for observed/loaded elements, might be limited to sensor role)
							if (!sfList.isEmpty())
							{
								try {
									IObjectSurrogate surr = sfList.get(0).getInstance(tObj);				
									TSurrogateInstanceRef ref = reg.registerSurrogate(surr, rdNew);
									surr.setSurrogateInstanceRef(ref);
									oc.setOperationalViaSurrogate(ref);
									//observables.add(surr.acquire(scope, oc)); lazy acquiring in surrogate
								} catch (InsufficientModelInformationException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}																
						}
					}
				}
				else
				{
					logger.warn(new FormattedMessage("Cannot resolve supplied TCollabObject {0} to type reference via ModelTypesUtil; ignoring event", tObj.getId()));										
				}
			}
			else 
			{
				logger.warn(new SimpleMessage("ObjectType of loaded elements is NULL but required in OperativeObjectEvent; ignoring event"));
				logger.debug(new FormattedMessage("Incomplete OperativeObjectEvent: {0}", e.toString()));				
			}
		}

		private void wireObject(OperativeObjectEvent e, TOperationalComponent oComp, TOperationalConnector oConn, TOperationalObject oObj, TOperationalObject targetObj, TCollabObject targetType)
		{
			if (oObj != null) // origin is an object
			{
				for (TObjectRef tRef : e.getLoadedViaRef())
				{
					model.addObjectRefIfNotExisting(oObj, targetObj, tRef, false);				
				}			
				if (!e.getLoadedViaSubstructureWireType().isEmpty()) 
				{
					logger.error("Loading via SubstructureWires is unsupported for now; ignoring event");
				}
			}
			else if (!(oComp == null && oConn == null)) // origin is comp or conn
			{ 
				for (TCollabLink linkType : e.getLoadedViaLinkType()) // cannot be both via ref and action as these connect different element types
				{ // origin is comp or conn:
					if (oComp != null)
						model.addLinkIfNotExisting(oComp, targetObj, linkType);
					else if (oConn != null)
						model.addLinkIfNotExisting(oConn, targetObj, linkType);
				}
				for (TAction actionType : e.getLoadedViaOppositeActionType()) // i.e. via an action of the source element
				{
					// TODO: later check whether the action provided is of type action or operative action --> later one needs to be resolved to former one
					TCollaborator sourceType = null;
					if (oComp != null)
						sourceType = (TCollaborator) mtu.getByTypeRef(oComp.getInstanceOf());
					if (oConn != null)
						sourceType = (TCollaborator) mtu.getByTypeRef(oConn.getInstanceOf());
					// establish Link/WireType
					List<TCollabLink> fromLinks = mtu.getLinkableWires(scope.getLinkRef(), sourceType);
					List<TCollabLink> toLinks = mtu.getLinkableWires(scope.getLinkRef(), targetType);
					fromLinks.retainAll(toLinks);		
					if (fromLinks.isEmpty())
						logger.warn(new FormattedMessage("No LinkType found that links {0} and {1} via an action.", sourceType.getId(), e.getObjType().getId()));
					boolean isFound = false;
					for (TCollabLink candidateLink : fromLinks)
					{
						if (candidateLink.getCollabActionEndpoint().equals(actionType))
						{
							if (oComp != null)
								model.addLinkIfNotExisting(oComp, targetObj, candidateLink);
							else if (oConn != null)
								model.addLinkIfNotExisting(oConn, targetObj, candidateLink);
							isFound = true;
							break;	
						}
					}
					if (!isFound)
						logger.warn(new FormattedMessage("None of the available LinkTypes between \"{0}\" and \"{1}\" contain the stated action \"{2}\".", new Object[]{sourceType.getId(), e.getObjType().getId(), actionType.getId()}));
				}			
			}
		}

		private void wireCollaborator(OperativeCollaboratorEvent e, TOperationalComponent oComp, TOperationalConnector oConn, TOperationalObject oObj, TOperationalComponent targetComp, TOperationalConnector targetConn)
		{
			if (!(oComp == null && oConn == null)) // origin is comp or conn
			{
				for (TCollabRef tRef : e.getLoadedViaRef())  
				{ 
					if (targetComp != null)
						model.addCollabRefIfNotExisting(oComp != null ? oComp : oConn, targetComp, tRef, false);
					if (targetConn != null)
						model.addCollabRefIfNotExisting(oComp != null ? oComp : oConn, targetConn, tRef, false);
				}
				if (!e.getLoadedViaSubstructureWireType().isEmpty())
				{
					logger.error("Loading via SubstructureWires is unsupported for now; ignoring event"); 
				}
			}							
			else if (oObj != null) { // source is an object
				for (TCollabLink linkType : e.getLoadedViaLinkType())
				{								
					if (targetComp != null)
						model.addLinkIfNotExisting(targetComp, oObj, linkType);
					if (targetConn != null)
						model.addLinkIfNotExisting(targetConn, oObj, linkType);
				}
				for (TAction actionType : e.getLoadedViaOppositeActionType()) // i.e., an object action
				{
					// TODO: later check whether the action provided is of type action or operative action --> later one needs to be resolved to former one
					TCollabObject sourceType = (TCollabObject) mtu.getByTypeRef(oObj.getInstanceOf());									
					// establish Link/WireType
					List<TCollabLink> fromLinks = mtu.getLinkableWires(scope.getLinkRef(), sourceType);
					List<TCollabLink> toLinks = mtu.getLinkableWires(scope.getLinkRef(), e.getCollaboratorType());
					fromLinks.retainAll(toLinks);	
					if (fromLinks.isEmpty())
						logger.warn(new FormattedMessage("No LinkType found that links {0} and {1} via an action.", sourceType.getId(), e.getCollaboratorType().getId()));
					boolean isFound = false;
					for (TCollabLink candidateLink : fromLinks)
					{
						if (candidateLink.getObjActionEndpoint().equals(actionType))
						{											
							if (targetComp != null)
								model.addLinkIfNotExisting(targetComp, oObj, candidateLink);
							if (targetConn != null)
								model.addLinkIfNotExisting(targetConn, oObj, candidateLink);
							isFound = true;
							break;	
						}
					}
					if (!isFound)
						logger.warn(new FormattedMessage("None of the available LinkTypes between \"{0}\" and \"{1}\" contain the stated action \"{2}\".", new Object[]{sourceType.getId(), e.getCollaboratorType().getId(), actionType.getId()}));
				}
			}
		}
		
		private void removeRelations(String sourceOperationalElement, Set<String> targetOperationalElements, SensingScope ofType)
		{
			if (ofType.isCompletelyEmpty())
				return;
			
			for (TCollabLink linkType : ofType.getLinks())
			{
				logger.warn(new MessageFormatMessage("Checking for Link Differences not implemented, thus not checked for linkType {0} between {1} and {2} ", linkType.getId(), sourceOperationalElement, targetOperationalElements.toArray().toString()));
			}
			for (TAction actionType : ofType.getActions())
			{
				logger.warn(new MessageFormatMessage("Checking for Link Differences not implemented, thus not checked for sourceActionType {0} between {1} and {2} ", actionType.getId(), sourceOperationalElement, targetOperationalElements.toArray().toString()));
			}
			if (!ofType.getSubstructureWires().isEmpty())
			{
				logger.warn("Sensing and Diffing SubstructureWires not supported");
			}
			
			THADLarchElement source = model.getOperationalElement(sourceOperationalElement);	
			for (String target : targetOperationalElements)
			{
				THADLarchElement targetEl = model.getOperationalElement(target);
				for (TObjectRef ref : ofType.getObjectRefs())
				{
					model.setObjectRefState(new ImmutableTriple<THADLarchElement, THADLarchElement, TObjectRef>(source, targetEl, ref), TOperationalState.DESCRIBED_NONEXISTING);
				}
				
				for (TCollabRef ref : ofType.getCollabRefs())
				{
					model.setCollabRefState(new ImmutableTriple<THADLarchElement, THADLarchElement, TCollabRef>(source, targetEl, ref), TOperationalState.DESCRIBED_NONEXISTING);
				}
			}												
		}
		
		private void removeRelations(String sourceOperationalElement, SensingScope ofType, Set<THADLarchElement> ignoringOperationalElements)
		{
			logger.warn(new MessageFormatMessage("Diffing of Relations to Non-processed/Preexisting Elements reachable from {0} not implemented yet", sourceOperationalElement));
		}
	}
}
