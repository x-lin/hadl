package at.ac.tuwien.dsg.hadl.framework.runtime.events;

public enum EventEffectType {
	
		Loaded,
		Added,
		Removed,
		Updated;
		
		
	    public String value() {
	        return name();
	    }

	    public static EventEffectType fromValue(String v) {
	        return valueOf(v);
	    }

			
}
