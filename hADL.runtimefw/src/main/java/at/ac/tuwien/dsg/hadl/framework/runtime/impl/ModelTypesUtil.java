package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.Map.Entry;

import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory;



public class ModelTypesUtil 
{
	private Hashtable<String,THADLarchElement> action2parent = new Hashtable<String,THADLarchElement>();	
	private Hashtable<THADLarchElement,THADLarchElement> elHierarchy = new Hashtable<THADLarchElement,THADLarchElement>();
	// doesn't include substructure to superstructure mapping, as structure may be used multiple times in a super structure
	private Hashtable<String,Entry<TTypeRef,THADLarchElement>> refIndex = new Hashtable<String,Entry<TTypeRef,THADLarchElement>>();
	private Hashtable<String,Entry<TTypeRef,THADLarchElement>> inverseRefIndex = new Hashtable<String,Entry<TTypeRef,THADLarchElement>>();
	
	public ModelTypesUtil()
	{
		
	}
	
	public void init(HADLmodel model)
	{	
		for (TStructure struct : model.getHADLstructure())
		{
			elHierarchy.put(struct, model);			
			processStructure(struct);
		}		
	}
	
	private void processStructure(TStructure struct)
	{
		if (struct == null)
			return;
		addToIndex(struct);
		for (THumanComponent el : struct.getComponent())
		{			
			elHierarchy.put(el, struct);
			addToIndex(el);
			// depth first
			if (el.getSubstructure() != null && el.getSubstructure().getSubstructureRef() != null) 
			{
				processStructure(el.getSubstructure().getSubstructureRef());
			}
			for (TAction action : el.getAction())
			{
				addToIndex(action);
				action2parent.put(action.getId(), el);
			}
		}
		for (TCollabConnector el : struct.getConnector())
		{
			elHierarchy.put(el, struct);
			addToIndex(el);
			// depth first
			if (el.getSubstructure() != null && el.getSubstructure().getSubstructureRef() != null) 
			{
				processStructure(el.getSubstructure().getSubstructureRef());
			}
			for (TAction action : el.getAction())
			{
				addToIndex(action);
				action2parent.put(action.getId(), el);
			}
		}
		for (TCollabObject el : struct.getObject())
		{
			elHierarchy.put(el, struct);
			addToIndex(el);
			// depth first
			if (el.getSubstructure() != null && el.getSubstructure().getSubstructureRef() != null) 
			{
				processStructure(el.getSubstructure().getSubstructureRef());
			}
			for (TAction action : el.getAction())
			{
				addToIndex(action);
				action2parent.put(action.getId(), el);
			}
		}
		for (TCollabLink link : struct.getLink())
		{
			addToIndex(link);
		}
		for(TCollabRef ref : struct.getCollabRef())
		{
			addToIndex(ref);
		}
		for(TObjectRef ref : struct.getObjectRef())
		{
			addToIndex(ref);
		}		
	}
	
	public List<TCollabLink> getLinkableWires(List<TCollabLink> wireTypes, TCollabObject objType)
	{
		List<TCollabLink> linkable = new ArrayList<TCollabLink>();
		for (TCollabLink link : wireTypes)
		{
			TCollabObject compareTo = getObjectForAction(link.getObjActionEndpoint());
			if (compareTo.equals(objType)){
				linkable.add(link);
			}
		}		
		return linkable;
	}
	
	public List<TCollabLink> getLinkableWires(List<TCollabLink> wireTypes, TCollaborator elType)
	{
		List<TCollabLink> linkable = new ArrayList<TCollabLink>();
		for (TCollabLink link : wireTypes)
		{
			TCollaborator compareTo = getCollaboratorForAction(link.getCollabActionEndpoint());
			if (compareTo.equals(elType)){
				linkable.add(link);
			}
		}		
		return linkable;
	}
	
	public TCollaborator getCollaboratorForAction(TAction action)
	{
		THADLarchElement el = action2parent.get(action.getId());
		if (el != null && el instanceof TCollaborator)
			return (TCollaborator)el;
		else 
			return null;
	}
	
	public TCollabObject getObjectForAction(TAction action)
	{
		THADLarchElement el = action2parent.get(action.getId());
		if (el != null && el instanceof TCollabObject)
			return (TCollabObject)el;
		else 
			return null;
	}
	
	public THADLarchElement getParentOf(THADLarchElement el)
	{
		return elHierarchy.get(el);
	}
	
	public TStructure getStructureParentOf(THADLarchElement el)
	{
		THADLarchElement pEl = elHierarchy.get(el);
		if (pEl != null && pEl instanceof TStructure)
			return (TStructure) pEl;
		else
			return null;
	}
	
	public THADLarchElement getByTypeRef(TTypeRef tRef)
	{
		if (inverseRefIndex.containsKey(tRef.getHref()))
		{
			return inverseRefIndex.get(tRef.getHref()).getValue();
		}
		return null;
	}
	
	public THADLarchElement getById(String id)
	{
		if (refIndex.containsKey(id))
		{
			return refIndex.get(id).getValue();
		}
		return null;
	}
	
	public TTypeRef lookupTypeRef(THADLarchElement element)
	{
		if (refIndex.containsKey(element.getId()))
		{
			return refIndex.get(element.getId()).getKey();
		}					
		return null;
	}
	
	private boolean addToIndex(THADLarchElement el)
	{
		if (el.getId() != null && !refIndex.containsKey(el.getId()))
		{
			Entry<TTypeRef, THADLarchElement> e = new AbstractMap.SimpleEntry<TTypeRef, THADLarchElement>(createRef(el),el);
			refIndex.put(el.getId(), e);
			inverseRefIndex.put(e.getKey().getHref(), e);
			return true;
		}
		return false;
	}
	
	private TTypeRef createRef(THADLarchElement element)
	{
		//TODO: LATER make id that can serve as HREF eventually
		String id = element.getId() != null ? element.getId() : UUID.randomUUID().toString();
		TTypeRef ref = new ObjectFactory().createTTypeRef();
		ref.setHref(id);
		return ref;
	}
	

}
