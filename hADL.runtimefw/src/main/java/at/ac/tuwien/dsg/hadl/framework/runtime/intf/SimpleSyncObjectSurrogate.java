package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import rx.Observable;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;

public class SimpleSyncObjectSurrogate implements IObjectSurrogate{

	protected TOperationalObject oc = null;
	protected TActivityScope scope = null;
	protected TSurrogateInstanceRef thisRef = null;

	public SimpleSyncObjectSurrogate() {
		super();
	}

	@Override
	public Observable<SurrogateEvent> begin() {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, oc));
	}

	@Override
	public Observable<SurrogateEvent> stop() {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, oc));
	}

	@Override
	public Observable<SurrogateEvent> release() {
		this.oc = null;
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS, oc));
	}

	@Override
	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalObject surrogateFor) {
		this.scope = forScope;
		this.oc = surrogateFor;
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, oc));
	}

	@Override
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalConnector oppositeElement, TAction oppositeAction, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalComponent oppositeElement, TAction oppositeAction, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalComponent oppositeElement, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalConnector oppositeElement, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> relating(TOperationalObject oppositeElement, TObjectRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalObjectRef ref) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
	}

	@Override
	public String toString() {
		String s1 = oc == null ? "none" : "["+ oc.getName() + " Id: "+ oc.getId()+ "]";  
		String s2 = scope == null ? "none" : "["+ scope.getName() + " Id: "+ scope.getId()+ "]";
		return "DefaultAcceptingCollaboratorSurrogate [oc=" + s1 + ", scope="
				+ s2 + "]";
	}

	@Override
	public void setSurrogateInstanceRef(TSurrogateInstanceRef ref) {
		this.thisRef = ref;
	}

}