//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.04.16 at 02:22:05 PM CEST 
//


package at.ac.tuwien.dsg.hadl.schema.executable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tAuthority complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tAuthority">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capabilityToCreate" type="{http://at.ac.tuwien.dsg/hADL/hADLexecutable}tAuthorityEndpoint" maxOccurs="unbounded"/>
 *         &lt;element name="capabilityToDestroy" type="{http://at.ac.tuwien.dsg/hADL/hADLexecutable}tAuthorityEndpoint" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tAuthority", propOrder = {
    "capabilityToCreate",
    "capabilityToDestroy"
})
public class TAuthority {

    @XmlElement(required = true)
    protected List<TAuthorityEndpoint> capabilityToCreate;
    @XmlElement(required = true)
    protected List<TAuthorityEndpoint> capabilityToDestroy;

    /**
     * Gets the value of the capabilityToCreate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the capabilityToCreate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapabilityToCreate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TAuthorityEndpoint }
     * 
     * 
     */
    public List<TAuthorityEndpoint> getCapabilityToCreate() {
        if (capabilityToCreate == null) {
            capabilityToCreate = new ArrayList<TAuthorityEndpoint>();
        }
        return this.capabilityToCreate;
    }

    /**
     * Gets the value of the capabilityToDestroy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the capabilityToDestroy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapabilityToDestroy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TAuthorityEndpoint }
     * 
     * 
     */
    public List<TAuthorityEndpoint> getCapabilityToDestroy() {
        if (capabilityToDestroy == null) {
            capabilityToDestroy = new ArrayList<TAuthorityEndpoint>();
        }
        return this.capabilityToDestroy;
    }

}
