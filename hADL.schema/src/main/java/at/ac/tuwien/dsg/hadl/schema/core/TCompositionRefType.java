//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.04.16 at 02:10:14 PM CEST 
//


package at.ac.tuwien.dsg.hadl.schema.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tCompositionRefType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tCompositionRefType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TO_INHERITS_FROM"/>
 *     &lt;enumeration value="FROM_CONTAINS_TO"/>
 *     &lt;enumeration value="FROM_DEPENDS_ON_TO"/>
 *     &lt;enumeration value="FROM_REFERENCES_TO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tCompositionRefType")
@XmlEnum
public enum TCompositionRefType {

    TO_INHERITS_FROM,
    FROM_CONTAINS_TO,
    FROM_DEPENDS_ON_TO,
    FROM_REFERENCES_TO;

    public String value() {
        return name();
    }

    public static TCompositionRefType fromValue(String v) {
        return valueOf(v);
    }

}
